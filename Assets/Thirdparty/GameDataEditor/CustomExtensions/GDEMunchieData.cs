// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by the Game Data Editor.
//
//      Changes to this file will be lost if the code is regenerated.
//
//      This file was generated from this data file:
//      /Users/Tomtominc/Documents/Development/MonsterPets/Assets/Thirdparty/GameDataEditor/Resources/gde_data.txt
//  </autogenerated>
// ------------------------------------------------------------------------------
using UnityEngine;
using System;
using System.Collections.Generic;

using GameDataEditor;

namespace GameDataEditor
{
    public class GDEMunchieData : IGDEData
    {
        static string dropRateKey = "dropRate";
		float _dropRate;
        public float dropRate
        {
            get { return _dropRate; }
            set {
                if (_dropRate != value)
                {
                    _dropRate = value;
					GDEDataManager.SetFloat(_key, dropRateKey, _dropRate);
                }
            }
        }

        static string idKey = "id";
		string _id;
        public string id
        {
            get { return _id; }
            set {
                if (_id != value)
                {
                    _id = value;
					GDEDataManager.SetString(_key, idKey, _id);
                }
            }
        }

        static string nameKey = "name";
		string _name;
        public string name
        {
            get { return _name; }
            set {
                if (_name != value)
                {
                    _name = value;
					GDEDataManager.SetString(_key, nameKey, _name);
                }
            }
        }

        static string displayNameKey = "displayName";
		string _displayName;
        public string displayName
        {
            get { return _displayName; }
            set {
                if (_displayName != value)
                {
                    _displayName = value;
					GDEDataManager.SetString(_key, displayNameKey, _displayName);
                }
            }
        }

        static string iconKey = "icon";
		string _icon;
        public string icon
        {
            get { return _icon; }
            set {
                if (_icon != value)
                {
                    _icon = value;
					GDEDataManager.SetString(_key, iconKey, _icon);
                }
            }
        }

        static string descriptionKey = "description";
		string _description;
        public string description
        {
            get { return _description; }
            set {
                if (_description != value)
                {
                    _description = value;
					GDEDataManager.SetString(_key, descriptionKey, _description);
                }
            }
        }

        public GDEMunchieData(string key) : base(key)
        {
            GDEDataManager.RegisterItem(this.SchemaName(), key);
        }
        public override Dictionary<string, object> SaveToDict()
		{
			var dict = new Dictionary<string, object>();
			dict.Add(GDMConstants.SchemaKey, "Munchie");
			
            dict.Merge(true, dropRate.ToGDEDict(dropRateKey));
            dict.Merge(true, id.ToGDEDict(idKey));
            dict.Merge(true, name.ToGDEDict(nameKey));
            dict.Merge(true, displayName.ToGDEDict(displayNameKey));
            dict.Merge(true, icon.ToGDEDict(iconKey));
            dict.Merge(true, description.ToGDEDict(descriptionKey));
            return dict;
		}

        public override void UpdateCustomItems(bool rebuildKeyList)
        {
        }

        public override void LoadFromDict(string dataKey, Dictionary<string, object> dict)
        {
            _key = dataKey;

			if (dict == null)
				LoadFromSavedData(dataKey);
			else
			{
                dict.TryGetFloat(dropRateKey, out _dropRate);
                dict.TryGetString(idKey, out _id);
                dict.TryGetString(nameKey, out _name);
                dict.TryGetString(displayNameKey, out _displayName);
                dict.TryGetString(iconKey, out _icon);
                dict.TryGetString(descriptionKey, out _description);
                LoadFromSavedData(dataKey);
			}
		}

        public override void LoadFromSavedData(string dataKey)
		{
			_key = dataKey;
			
            _dropRate = GDEDataManager.GetFloat(_key, dropRateKey, _dropRate);
            _id = GDEDataManager.GetString(_key, idKey, _id);
            _name = GDEDataManager.GetString(_key, nameKey, _name);
            _displayName = GDEDataManager.GetString(_key, displayNameKey, _displayName);
            _icon = GDEDataManager.GetString(_key, iconKey, _icon);
            _description = GDEDataManager.GetString(_key, descriptionKey, _description);
        }

        public GDEMunchieData ShallowClone()
		{
			string newKey = Guid.NewGuid().ToString();
			GDEMunchieData newClone = new GDEMunchieData(newKey);

            newClone.dropRate = dropRate;
            newClone.id = id;
            newClone.name = name;
            newClone.displayName = displayName;
            newClone.icon = icon;
            newClone.description = description;

            return newClone;
		}

        public GDEMunchieData DeepClone()
		{
			GDEMunchieData newClone = ShallowClone();
            return newClone;
		}

        public void Reset_dropRate()
        {
            GDEDataManager.ResetToDefault(_key, dropRateKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetFloat(dropRateKey, out _dropRate);
        }

        public void Reset_id()
        {
            GDEDataManager.ResetToDefault(_key, idKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(idKey, out _id);
        }

        public void Reset_name()
        {
            GDEDataManager.ResetToDefault(_key, nameKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(nameKey, out _name);
        }

        public void Reset_displayName()
        {
            GDEDataManager.ResetToDefault(_key, displayNameKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(displayNameKey, out _displayName);
        }

        public void Reset_icon()
        {
            GDEDataManager.ResetToDefault(_key, iconKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(iconKey, out _icon);
        }

        public void Reset_description()
        {
            GDEDataManager.ResetToDefault(_key, descriptionKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(descriptionKey, out _description);
        }

        public void ResetAll()
        {
            GDEDataManager.ResetToDefault(_key, idKey);
            GDEDataManager.ResetToDefault(_key, nameKey);
            GDEDataManager.ResetToDefault(_key, displayNameKey);
            GDEDataManager.ResetToDefault(_key, iconKey);
            GDEDataManager.ResetToDefault(_key, dropRateKey);
            GDEDataManager.ResetToDefault(_key, descriptionKey);


            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            LoadFromDict(_key, dict);
        }
    }
}
