﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TrainerModel
{
    public string name;
    public bool hired;
    public bool elite;
    public int level;
    public bool adventuring;
    public long adventureTime;
    
    public List<string> monsters;

    public TrainerModel()
    {
        monsters = new List<string>();
    }
}
