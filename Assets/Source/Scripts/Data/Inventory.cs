﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory
{
    public List<InventoryItem> items;

    public Inventory()
    {
        items = new List<InventoryItem>();    
    }

    public void modifyItem(string p_item, long p_amount)
    {
        InventoryItem l_item = items.Find(x => x.name == p_item);

        if (l_item == null)
        {
            l_item = new InventoryItem();
            l_item.name = p_item;
            items.Add(l_item);
        }

        l_item.amount += p_amount;

        if (l_item.amount < 0)
        {
            l_item.amount = 0;
        }
    }

    public InventoryItem getItem(string p_item)
    {
        InventoryItem l_item = items.Find(x => x.name == p_item);

        if (l_item == null)
        {
            l_item = new InventoryItem();
            l_item.name = p_item;
            items.Add(l_item);
        }

        return l_item;
    }
}
