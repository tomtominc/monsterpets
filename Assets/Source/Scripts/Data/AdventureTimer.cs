﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdventureTimer
{
    public string id;
    public long reward;
    public float duration;
    public float time;
    public bool auto;
    public bool active;

    public AdventureTimer(string p_id, long p_reward, float p_duration, bool p_auto)
    {
        id = p_id;
        reward = p_reward;
        duration = p_duration;
        auto = p_auto;
        active = false;
    }

    public void setValues(long p_reward, float p_duration, bool p_auto)
    {
        reward = p_reward;
        duration = p_duration;
        auto = p_auto;
    }
}
