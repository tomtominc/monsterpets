﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDataEditor;

[System.Serializable]
public class PlayerModel
{
    public Inventory inventory;
    public List<TrainerModel> trainers;

    public PlayerModel()
    {
        inventory = new Inventory();
        trainers = new List<TrainerModel>();
    }

    public void configureTrainers(List<GDETrainerData> p_trainerDatas)
    {
        for (int i = 0; i < p_trainerDatas.Count; i++)
        {
            GDETrainerData l_trainerData = p_trainerDatas[i];
            TrainerModel l_trainer = trainers.Find(x => x.name == l_trainerData.id);

            if (l_trainer != null)
                continue;

            l_trainer = new TrainerModel();
            l_trainer.name = l_trainerData.id;
            trainers.Add(l_trainer);
        }
    }
}
