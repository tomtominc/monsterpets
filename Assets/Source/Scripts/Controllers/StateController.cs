﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateController : Controller
{
    public override void OnApplicationStart()
    {
        m_states = new Dictionary<StateDefinition, State>();

        m_states.Add(StateDefinition.LOADING, new LoadingState(m_controller));
        m_states.Add(StateDefinition.MAIN, new MainState(m_controller));
        m_states.Add(StateDefinition.CAPSULE_MACHINE, new CapsuleMachineState(m_controller));
        m_states.Add(StateDefinition.DEX, new DexState(m_controller));
        m_states.Add(StateDefinition.INVENTORY, new InventoryState(m_controller));
        m_states.Add(StateDefinition.MORPH, new MorphState(m_controller));
        m_states.Add(StateDefinition.LOOT, new OpenLootState(m_controller));
        m_states.Add(StateDefinition.RETURN, new ReturnState(m_controller));
        m_states.Add(StateDefinition.SHOP, new ShopState(m_controller));

        m_states.Add(StateDefinition.TUTORIAL_ONBOARDING, new TutorialOnBoardingState(m_controller));

        m_states[m_currentState].enter();
    }

    private void Update()
    {
        m_states[m_currentState].update();
    }

    public void changeState(StateDefinition p_state)
    {
        m_states[m_currentState].exit();
        m_currentState = p_state;
        m_states[m_currentState].enter();
    }

    private StateDefinition m_currentState;
    private Dictionary<StateDefinition, State> m_states;
}
