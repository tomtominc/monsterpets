﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDataEditor;
using System;

public class TimeController : Controller
{
    public event Action<AdventureTimer> timerFinished;

    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);

        m_dataController = m_controller.getController<DataController>();
        m_timers = new List<AdventureTimer>();
        m_timersById = new Dictionary<string, AdventureTimer>();

    }

    public override void OnApplicationStart()
    {
        m_playerModel = m_dataController.playerModel;

        configureTimers();
    }

    public void addAdventureTimer(AdventureTimer p_timer)
    {
        if (!m_timersById.ContainsKey(p_timer.id))
        {
            m_timers.Add(p_timer);
            m_timersById.Add(p_timer.id, p_timer);
        }
    }

    public void Update()
    {
        for (int i = m_timers.Count - 1; i > -1; i--)
        {
            AdventureTimer l_timer = m_timers[i];

            if (l_timer.auto || l_timer.active)
            {
                l_timer.time += Time.deltaTime;

                if (l_timer.time >= l_timer.duration)
                {
                    l_timer.time = 0;
                    l_timer.active = l_timer.auto;

                    timerFinished(l_timer);
                }
            }
        }
    }

    public AdventureTimer getTimer(TrainerModel l_trainer, GDETrainerData l_data)
    {
        AdventureTimer l_timer = null;

        if (m_timersById.ContainsKey(l_data.id))
        {
            l_timer = m_timersById[l_data.id];
            long l_reward = Formula.getRewardValue(l_trainer, l_data);
            l_timer.setValues(l_reward, l_data.duration, l_trainer.hired && l_trainer.elite);
        }
        else
        {
            long l_reward = Formula.getRewardValue(l_trainer, l_data);
            l_timer = new AdventureTimer(l_data.id, l_reward, l_data.duration, l_trainer.hired && l_trainer.elite);
            addAdventureTimer(l_timer);
        }

        return l_timer;
    }

    private void configureTimers()
    {
        List<TrainerModel> l_trainers = m_playerModel.trainers;

        for (int i = 0; i < l_trainers.Count; i++)
        {
            TrainerModel l_trainer = l_trainers[i];

            GDETrainerData l_data = new GDETrainerData(l_trainer.name);

            long l_reward = Formula.getRewardValue(l_trainer, l_data);

            AdventureTimer l_timer = new AdventureTimer(l_data.id,
                                                        l_reward,
                                                        l_data.duration,
                                                        l_trainer.hired && l_trainer.elite);
            addAdventureTimer(l_timer);
        }
    }

    private DataController m_dataController;
    private PlayerModel m_playerModel;
    private List<AdventureTimer> m_timers;
    private Dictionary<string, AdventureTimer> m_timersById;
}
