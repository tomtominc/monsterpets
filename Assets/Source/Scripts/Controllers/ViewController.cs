﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewController : Controller
{
    [SerializeField]
    private RectTransform m_viewCanvas;

    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);
        m_open = new Dictionary<ViewDefinition, View>();
        m_views = new Dictionary<ViewDefinition, RectTransform>();

        m_views.Add(ViewDefinition.MAIN, Resources.Load<RectTransform>("Views/MainMenu"));
        m_views.Add(ViewDefinition.MAIN_HUD, Resources.Load<RectTransform>("Views/MainHud"));
        m_views.Add(ViewDefinition.NAVIGATION_BAR, Resources.Load<RectTransform>("Views/NavigationController"));
        m_views.Add(ViewDefinition.DEX, Resources.Load<RectTransform>("Views/DexMenu"));
        m_views.Add(ViewDefinition.SHOP, Resources.Load<RectTransform>("Views/ShopMenu"));
        m_views.Add(ViewDefinition.CAPSULE_MACHINE, Resources.Load<RectTransform>("Views/MachineMenu"));
        m_views.Add(ViewDefinition.INVENTORY, Resources.Load<RectTransform>("Views/BagMenu"));
        m_views.Add(ViewDefinition.DIALOGUE, Resources.Load<RectTransform>("Views/DialogueView"));
    }

    public View open(ViewDefinition p_definition)
    {
        View l_view = null;

        if (m_open.ContainsKey(p_definition))
        {
            l_view = m_open[p_definition];
        }
        else
        {
            RectTransform l_viewRect = Instantiate(m_views[p_definition]);
            l_viewRect.SetParent(m_viewCanvas, false);
            l_view = l_viewRect.GetComponent<View>();
            l_view.initialize(m_controller);
            l_view.open();

            m_open.Add(p_definition, l_view);
        }

        return l_view;
    }

    public void close(ViewDefinition p_definition)
    {
        if (m_open.ContainsKey(p_definition))
        {
            View l_view = m_open[p_definition];
            l_view.close();
            m_open.Remove(p_definition);

            Debug.LogFormat("Trying to close {0}", p_definition);
        }
        else
        {
            Debug.LogFormat("Open does not contain {0}", p_definition);
        }
    }

    private Dictionary<ViewDefinition, RectTransform> m_views;
    private Dictionary<ViewDefinition, View> m_open;
}
