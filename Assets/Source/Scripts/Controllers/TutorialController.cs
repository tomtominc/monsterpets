﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : Controller
{
    public List<Dialogue> tutorials;

    private void Update()
    {
    }

    public Dialogue getTutorial(int p_index)
    {
        return tutorials[p_index];

    }
}
