﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDataEditor;

public class DataController : Controller
{
    public const string DATA_FILE = "gde_data";
    public const string PLAYER_DATA_FILE = "player_data_v0.1.1";

    public PlayerModel playerModel
    {
        get { return m_playerModel; }
    }

    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);

        GDEDataManager.Init(DATA_FILE);
        loadPlayer();
        loadMonsters();
    }

    public List<GDETrainerData> getTrainers()
    {
        return GDEDataManager.GetAllItems<GDETrainerData>();
    }

    public GDEMonsterData getMonster(string p_id)
    {
        return m_monsters[p_id];
    }

    public GDEMonsterData getRandomMonster()
    {
        Selector l_selector = new Selector();
        ItemSelection l_selection = l_selector.Single(m_monstersWeighted);
        return getMonster(l_selection.id);
    }

    public void save()
    {
        string l_playerFile = JsonUtility.ToJson(m_playerModel);
        PlayerPrefs.SetString(PLAYER_DATA_FILE, l_playerFile);
        PlayerPrefs.Save();
    }

    private void loadPlayer()
    {
        if (PlayerPrefs.HasKey(PLAYER_DATA_FILE))
        {
            string l_playerFile = PlayerPrefs.GetString(PLAYER_DATA_FILE);
            m_playerModel = JsonUtility.FromJson<PlayerModel>(l_playerFile);
        }
        else
        {
            m_playerModel = new PlayerModel();
            string l_playerFile = JsonUtility.ToJson(m_playerModel);
            PlayerPrefs.SetString(PLAYER_DATA_FILE, l_playerFile);
        }

        m_playerModel.configureTrainers(getTrainers());

        save();
    }

    private void loadMonsters()
    {
        m_monsters = new Dictionary<string, GDEMonsterData>();
        m_monstersWeighted = new List<ItemSelection>();

        List<GDEMonsterData> l_monsters = GDEDataManager.GetAllItems<GDEMonsterData>();

        for (int i = 0; i < l_monsters.Count; i++)
        {
            m_monsters.Add(l_monsters[i].id, l_monsters[i]);
            ItemSelection l_selection = new ItemSelection(l_monsters[i].id, l_monsters[i].dropRate);
            m_monstersWeighted.Add(l_selection);
        }
    }

    private PlayerModel m_playerModel;
    private Dictionary<string, GDEMonsterData> m_monsters;

    private List<ItemSelection> m_monstersWeighted;
    private List<ItemSelection> m_munchiesWeighted;
}
