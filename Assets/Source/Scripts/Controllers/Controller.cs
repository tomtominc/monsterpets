﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public virtual void initialize(ApplicationController p_controller)
    {
        m_controller = p_controller;
    }

    public virtual void OnApplicationStart()
    {

    }


    public virtual void OnApplicationQuit()
    {

    }

    protected ApplicationController m_controller;
}
