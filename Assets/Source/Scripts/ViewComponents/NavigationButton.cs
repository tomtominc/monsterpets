﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class NavigationButton : MonoBehaviour
{
    public const float SHOWN_SIZE = 180F;
    public const float HIDE_SIZE = 120F;

    public const float ICON_SHOWN_SIZE = 100F;
    public const float ICON_HIDE_SIZE = 80F;

    [SerializeField]
    private Image m_icon;
    [SerializeField]
    private Text m_navigationLabel;
    [SerializeField]
    private LayoutElement m_layoutElement;
    [SerializeField]
    private Image m_background;
    [SerializeField]
    private Color m_onColor;
    [SerializeField]
    private Color m_offColor;

    public void initialize(ApplicationController p_controller)
    {
        //m_controller = p_controller;
        m_icon.rectTransform.anchoredPosition = new Vector2(m_icon.rectTransform.anchoredPosition.x, 10f);
    }

    public void updateView(bool p_isShown)
    {
        if (p_isShown)
        {
            animateShown();
        }
        else
        {
            animateHide();
        }
    }

    private void animateShown()
    {
        float l_duration = 0.5f;

        m_icon.DOFade(1f, l_duration);
        m_navigationLabel.DOFade(1f, l_duration);
        m_background.DOColor(m_onColor, l_duration);
        m_layoutElement.DOPreferredSize(new Vector2(
            SHOWN_SIZE,
            m_layoutElement.preferredHeight), l_duration);

        RectTransform l_iconRect = m_icon.transform as RectTransform;
        l_iconRect.DOSizeDelta(Vector2.one * ICON_SHOWN_SIZE, l_duration);
        l_iconRect.DOAnchorPos(new Vector2(l_iconRect.anchoredPosition.x, 30f), l_duration)
                  .SetEase(Ease.OutBack, 2);

        RectTransform l_navigationLabelRect = m_navigationLabel.transform as RectTransform;
        l_navigationLabelRect.anchoredPosition = new Vector2(0f, -4f);
        l_navigationLabelRect.DOAnchorPos(Vector2.zero, l_duration);
    }

    private void animateHide()
    {
        float l_duration = 0.5f;

        m_icon.DOFade(0.75f, l_duration);
        m_navigationLabel.DOFade(0f, l_duration);
        m_background.DOColor(m_offColor, l_duration);
        m_layoutElement.DOPreferredSize(new Vector2(
            HIDE_SIZE,
            m_layoutElement.preferredHeight), l_duration);

        RectTransform l_iconRect = m_icon.transform as RectTransform;
        l_iconRect.DOSizeDelta(Vector2.one * ICON_HIDE_SIZE, l_duration);
        l_iconRect.DOAnchorPos(new Vector2(l_iconRect.anchoredPosition.x, 10f), l_duration)
                  .SetEase(Ease.OutBounce);

        RectTransform l_navigationLabelRect = m_navigationLabel.transform as RectTransform;
        l_navigationLabelRect.DOAnchorPos(new Vector2(0f, -4f), l_duration);
    }

    private void onClick()
    {
        // snapTo(m_navigationView.transform as RectTransform);
        //  m_navigationView.forceOn();
    }

    private Tweener snapTo(RectTransform target)
    {
        //Canvas.ForceUpdateCanvases();

        //Vector2 l_contentPoint = (Vector2)m_navigationScrollRect.transform.InverseTransformPoint(m_navigationScrollRect.content.position);
        //Vector2 l_itemPoint = (Vector2)m_navigationScrollRect.transform.InverseTransformPoint(target.position);
        //Vector2 l_targetPoint = l_contentPoint - l_itemPoint;

        //return m_navigationScrollRect.content.DOAnchorPos(l_targetPoint, 2000f).SetSpeedBased(true).SetEase(Ease.OutBack);

        return null;
    }

    //private ApplicationController m_controller;

}
