﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

[RequireComponent(typeof(Button))]
public class PurchaseButton : MonoBehaviour
{
    [SerializeField]
    private Button m_button;
    [SerializeField]
    private Text m_titleLabel;
    [SerializeField]
    private Text m_costLabel;

    public event Action purchaseAction = delegate { };
    public event Action insufficentAction = delegate { };

    public bool purchasable
    {
        get { return m_purchasable; }
    }

    public void initialize(ApplicationController p_controller)
    {
        m_controller = p_controller;
        m_dataController = m_controller.getController<DataController>();
        m_playerModel = m_dataController.playerModel;
    }

    public void setTitle(string p_title)
    {
        m_titleLabel.text = p_title;
    }

    public void setCurrency(string p_currency, long p_amount)
    {
        m_currency = p_currency;
        m_amount = p_amount;

        m_costLabel.text = NumberUtility.format(m_amount);
        Inventory l_inventory = m_playerModel.inventory;
        InventoryItem l_currency = l_inventory.getItem(m_currency);
        m_purchasable = l_currency.amount >= m_amount;

        if (m_purchasable)
        {
            configurePurchasable();
        }
        else
        {
            configureInsufficent();
        }
    }

    public void removeListeners()
    {
        m_button.onClick.RemoveAllListeners();
    }

    private void configurePurchasable()
    {
        removeListeners();

        m_costLabel.color = Color.white;
        m_button.onClick.AddListener(onPurchaseButton);
    }


    private void configureInsufficent()
    {
        removeListeners();

        m_costLabel.color = Color.red;
        m_button.onClick.AddListener(onInsufficentButton);
    }


    private void onPurchaseButton()
    {
        Inventory l_inventory = m_playerModel.inventory;
        l_inventory.modifyItem(m_currency, -m_amount);

        purchaseAction();
    }

    private void onInsufficentButton()
    {
        insufficentAction();
    }

    private ApplicationController m_controller;
    private DataController m_dataController;
    private PlayerModel m_playerModel;

    private string m_currency;
    private long m_amount;
    private bool m_purchasable;
}
