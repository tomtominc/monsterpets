﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AdventureTimerView : MonoBehaviour
{
    [SerializeField]
    private Slider m_timeSlider;
    [SerializeField]
    private Text m_rewardLabel;
    [SerializeField]
    private Text m_timeLabel;

    public AdventureTimer timer
    {
        get { return m_timer; }
    }

    public void setTimer(AdventureTimer p_timer)
    {
        m_timer = p_timer;
        m_timeSlider.maxValue = m_timer.duration;
        m_timeSlider.minValue = 0;
        m_timeSlider.value = 0;
    }

    private void Update()
    {
        if (null != m_timer)
        {
            m_rewardLabel.text = NumberUtility.format(m_timer.reward);
            m_timeSlider.value = m_timer.time;
            m_timeLabel.text = NumberUtility.timeFormat(m_timer.duration - m_timer.time);
        }
    }

    private AdventureTimer m_timer;
}
