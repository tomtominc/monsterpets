﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameDataEditor;
using System;

public class TrainerView : MonoBehaviour
{
    public const float TRAINER_UPPER_CONTENT_HEIGHT = 82F;
    public const float TRAINER_CONTENT_MAX_HEIGHT = 228F;
    public const float TRAINER_CONTENT_INFO_HEIGHT = 140F;
    public const float TRAINER_CONTENT_CLOSED_HEIGHT = 84F;

    [SerializeField]
    protected Image m_icon;
    [SerializeField]
    protected Text m_nameLabel;
    [SerializeField]
    protected Text m_descriptionLabel;
    [SerializeField]
    protected Text m_levelLabel;
    [SerializeField]
    protected AdventureTimerView m_timerView;
    [SerializeField]
    protected RectTransform m_lowerContentRect;
    [SerializeField]
    protected RectTransform m_monsterViewContent;
    [SerializeField]
    protected RectTransform m_descriptionContent;
    [SerializeField]
    protected LayoutElement m_layoutElement;
    [SerializeField]
    protected CanvasGroup m_canvasGroup;

    [Header("Buttons")]
    [SerializeField]
    protected PurchaseButton m_hireButton;
    [SerializeField]
    protected PurchaseButton m_levelUpButton;
    [SerializeField]
    protected Button m_adventureButton;

    [Header("Colorable Items")]
    [SerializeField]
    protected Image m_folder;
    [SerializeField]
    protected Image m_background;

    [Header("Effects")]
    [SerializeField]
    protected GameObject m_sleepEffect;

    [Header("Colors")]
    [SerializeField]
    protected Color m_redColor;
    [SerializeField]
    protected Color m_blueColor;
    [SerializeField]
    protected Color m_inactiveColor;

    public TrainerModel trainer
    {
        get { return m_trainer; }
    }

    public GDETrainerData data
    {
        get { return m_data; }
    }

    public Canvas canvas
    {
        get { return GetComponent<Canvas>(); }
    }

    public PurchaseButton hireButton
    {
        get { return m_hireButton; }
    }

    public PurchaseButton levelUpButton
    {
        get { return m_levelUpButton; }
    }
    public Button adventureButton
    {
        get { return m_adventureButton; }
    }

    public event Action<TrainerView> adventureAction = delegate { };
    public event Action<TrainerView> purchaseTrainer = delegate { };
    public event Action insufficentAction = delegate { };

    public event Action<TrainerView> purchaseCaptureMonster = delegate { };
    public event Action<TrainerView, TrainerPetView> monsterShow = delegate { };

    public void Initialize(ApplicationController p_controller, TrainerModel p_trainer, GDETrainerData p_data)
    {
        m_controller = p_controller;
        m_timeController = m_controller.getController<TimeController>();
        m_dataController = m_controller.getController<DataController>();
        m_trainer = p_trainer;
        m_data = p_data;

        m_icon.sprite = Resources.Load<Sprite>(m_data.icon);
        m_nameLabel.text = m_data.displayName;
        m_descriptionLabel.text = m_data.description;

        m_levelUpButton.initialize(m_controller);
        m_hireButton.initialize(m_controller);

        m_adventureButton.onClick.AddListener(onAdventureAction);

        initializeMonsterViews();

        reload();
    }

    public void reload()
    {
        configurePurchaseButtons();
        configureTimerView();
        configureLowerContent();
        configureColor();

        m_levelLabel.text = string.Format("Lv {0:D3}", m_trainer.level);
    }

    public void configurePurchaseButtons()
    {
        if (m_trainer.hired)
        {
            m_sleepEffect.gameObject.SetActive(!m_timerView.timer.active);

            m_hireButton.gameObject.SetActive(false);
            m_levelUpButton.gameObject.SetActive(true);
            m_levelUpButton.setCurrency(Currency.COIN, Formula.getLevelUpCost(m_trainer, m_data));
            m_levelUpButton.purchaseAction -= onPurchaseTrainer;
            m_levelUpButton.insufficentAction -= onInsufficentAction;
            m_levelUpButton.purchaseAction += onPurchaseTrainer;
            m_levelUpButton.insufficentAction += onInsufficentAction;

            configureMonsters();
        }
        else
        {
            m_sleepEffect.gameObject.SetActive(false);

            m_levelUpButton.gameObject.SetActive(false);
            m_hireButton.gameObject.SetActive(true);
            m_hireButton.setCurrency(Currency.COIN, Formula.getLevelUpCost(m_trainer, m_data));
            m_hireButton.purchaseAction -= onPurchaseTrainer;
            m_hireButton.insufficentAction -= onInsufficentAction;
            m_hireButton.purchaseAction += onPurchaseTrainer;
            m_hireButton.insufficentAction += onInsufficentAction;

            if (m_hireButton.purchasable)
            {
                m_canvasGroup.alpha = 1f;
            }
            else
            {
                m_canvasGroup.alpha = 0.5f;
            }
        }
    }

    private void configureTimerView()
    {
        AdventureTimer l_timer = m_timeController.getTimer(m_trainer, m_data);

        if (l_timer != null && m_timerView.timer == null)
        {
            m_timerView.setTimer(l_timer);
        }
    }

    private void configureLowerContent()
    {
        if (m_trainer.hired)
        {
            m_layoutElement.preferredHeight = TRAINER_CONTENT_MAX_HEIGHT;
            m_descriptionContent.gameObject.SetActive(false);
            m_monsterViewContent.gameObject.SetActive(true);
            m_timerView.gameObject.SetActive(true);
        }
        else
        {
            m_layoutElement.preferredHeight = TRAINER_CONTENT_INFO_HEIGHT;
            m_descriptionContent.gameObject.SetActive(true);
            m_monsterViewContent.gameObject.SetActive(false);
            m_timerView.gameObject.SetActive(false);
        }
    }

    private void configureColor()
    {
        if (m_trainer.hired)
        {
            Color l_color = transform.GetSiblingIndex() % 2 == 0 ? m_redColor : m_blueColor;
            m_folder.color = l_color;
            m_background.color = l_color;
        }
        else
        {
            m_folder.color = m_inactiveColor;
            m_background.color = m_inactiveColor;
        }
    }

    private void initializeMonsterViews()
    {
        for (int i = 0; i < m_monsterViewContent.childCount; i++)
        {
            Transform l_petTransform = m_monsterViewContent.GetChild(i);
            TrainerPetView l_petView = l_petTransform.GetComponent<TrainerPetView>();
            l_petView.initialize(m_controller, m_trainer, m_data);
        }
    }

    private void configureMonsters()
    {
        List<string> l_monsters = m_trainer.monsters;

        for (int i = 0; i < m_monsterViewContent.childCount; i++)
        {
            Transform l_petTransform = m_monsterViewContent.GetChild(i);
            TrainerPetView l_petView = l_petTransform.GetComponent<TrainerPetView>();

            l_petView.capturePurchased -= onPurchaseCapture;
            l_petView.captureInsufficent -= onInsufficentAction;
            l_petView.monsterShow -= onMonsterShow;

            if (i > l_monsters.Count)
            {
                l_petView.configureInactiveView();
            }
            else if (i == l_monsters.Count)
            {
                l_petView.capturePurchased += onPurchaseCapture;
                l_petView.captureInsufficent += onInsufficentAction;
                l_petView.configurePurchaseView();
            }
            else
            {
                l_petView.monsterShow += onMonsterShow;
                string l_monsterId = l_monsters[i];
                GDEMonsterData l_monsterData = m_dataController.getMonster(l_monsterId);
                l_petView.configureActiveView(l_monsterData);
            }
        }
    }

    private void onAdventureAction()
    {
        m_sleepEffect.gameObject.SetActive(false);

        adventureAction(this);
    }

    private void onPurchaseTrainer()
    {
        purchaseTrainer(this);
    }

    private void onPurchaseCapture()
    {
        purchaseCaptureMonster(this);
    }

    private void onInsufficentAction()
    {
        insufficentAction();
    }

    private void onMonsterShow(TrainerPetView p_monsterView)
    {
        monsterShow(this, p_monsterView);
    }

    private ApplicationController m_controller;
    private DataController m_dataController;
    private TimeController m_timeController;
    private PlayerModel m_playerModel;
    private TrainerModel m_trainer;
    private GDETrainerData m_data;


}
