﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class MonsterButtonView : MonoBehaviour
{
    [SerializeField]
    private RectTransform m_contentRect;
    [SerializeField]
    private RectTransform m_arrowRect;
    [SerializeField]
    private Image m_background;
    [SerializeField]
    private RectTransform m_mask;

    [SerializeField]
    private Button m_closeButton;
    [SerializeField]
    private Button m_morphButton;
    [SerializeField]
    private PurchaseButton m_tradeButton;

    //public event Action morphAction = delegate { };
    //public event Action tradeAction = delegate { };
    public event Action closeAction = delegate { };

    public void initialize(ApplicationController p_controller)
    {
        m_controller = p_controller;
        m_tradeButton.initialize(m_controller);
    }

    public void open(TrainerPetView p_petView)
    {
        m_petView = p_petView;
        RectTransform l_petRect = (RectTransform)m_petView.transform;
        m_mask.position = l_petRect.position;
        m_arrowRect.position = new Vector2(l_petRect.position.x, m_arrowRect.position.y);
        m_closeButton.onClick.AddListener(close);

        float l_alphaTo = m_background.color.a;
        float l_duration = 0.3f;

        m_contentRect.localScale = Vector3.zero;

        m_background.color = new Color(m_background.color.r,
                                       m_background.color.g,
                                       m_background.color.b, 0f);

        m_background.DOFade(l_alphaTo, l_duration);
        m_contentRect.DOScale(Vector3.one, l_duration).SetEase(Ease.OutBack);
    }

    public void close()
    {
        m_closeButton.onClick.RemoveListener(close);

        float l_alphaTo = m_background.color.a;
        float l_duration = 0.3f;
        m_background.DOFade(0f, l_duration);
        m_contentRect.DOScale(Vector3.zero, l_duration).SetEase(Ease.InBack)
                     .OnComplete(() =>
        {
            closeAction();
            m_background.color = new Color(m_background.color.r,
                                      m_background.color.g,
                                           m_background.color.b, l_alphaTo);
            m_contentRect.localScale = Vector3.one;
        });
    }

    private ApplicationController m_controller;
    private TrainerPetView m_petView;
}
