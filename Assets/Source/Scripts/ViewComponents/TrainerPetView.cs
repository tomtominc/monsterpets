﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameDataEditor;
using System;

public class TrainerPetView : MonoBehaviour
{
    [SerializeField]
    private RectTransform m_activeView;
    [SerializeField]
    private RectTransform m_inactiveView;
    [SerializeField]
    private PurchaseButton m_captureButton;
    [SerializeField]
    private RectTransform m_multiplierView;
    [SerializeField]
    private Button m_monsterShowButton;

    [SerializeField]
    private Image m_monsterIcon;
    [SerializeField]
    private Text m_multiplierLabel;

    public event Action capturePurchased = delegate { };
    public event Action captureInsufficent = delegate { };
    public event Action<TrainerPetView> monsterShow = delegate { };

    public void initialize(ApplicationController p_controller, TrainerModel p_trainer, GDETrainerData p_data)
    {
        m_controller = p_controller;
        m_captureButton.initialize(m_controller);

        m_trainer = p_trainer;
        m_trainerData = p_data;

        m_monsterShowButton.onClick.AddListener(onMonsterShow);
    }

    public void configureActiveView(GDEMonsterData p_data)
    {
        m_data = p_data;

        m_activeView.gameObject.SetActive(true);
        m_multiplierView.gameObject.SetActive(true);
        m_monsterShowButton.gameObject.SetActive(true);

        m_inactiveView.gameObject.SetActive(false);
        m_captureButton.gameObject.SetActive(false);

        m_monsterIcon.sprite = Resources.Load<Sprite>(m_data.icon);
        m_multiplierLabel.text = string.Format("x{0}", m_data.coefficient);

        m_captureButton.purchaseAction -= onCapturePurchased;
        m_captureButton.insufficentAction -= onCaptureInsufficent;
    }

    public void configurePurchaseView()
    {
        m_activeView.gameObject.SetActive(false);
        m_multiplierView.gameObject.SetActive(false);
        m_monsterShowButton.gameObject.SetActive(false);

        m_inactiveView.gameObject.SetActive(true);
        m_captureButton.gameObject.SetActive(true);

        m_captureButton.purchaseAction -= onCapturePurchased;
        m_captureButton.insufficentAction -= onCaptureInsufficent;
        m_captureButton.purchaseAction += onCapturePurchased;
        m_captureButton.insufficentAction += onCaptureInsufficent;

        long l_captureCost = Formula.getCaptureCost(m_trainer, m_trainerData);
        m_captureButton.setCurrency(Currency.COIN, l_captureCost);
    }

    public void configureInactiveView()
    {
        m_activeView.gameObject.SetActive(false);
        m_multiplierView.gameObject.SetActive(false);

        m_inactiveView.gameObject.SetActive(true);
        m_captureButton.gameObject.SetActive(false);

        m_captureButton.purchaseAction -= onCapturePurchased;
        m_captureButton.insufficentAction -= onCaptureInsufficent;
    }

    private void onCapturePurchased()
    {
        capturePurchased();
    }

    private void onCaptureInsufficent()
    {
        captureInsufficent();
    }

    private void onMonsterShow()
    {
        monsterShow(this);
    }

    private ApplicationController m_controller;
    private TrainerModel m_trainer;
    private GDETrainerData m_trainerData;
    private GDEMonsterData m_data;

}
