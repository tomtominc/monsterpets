﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingState : State
{
    public LoadingState(ApplicationController p_controller)
        : base(p_controller)
    {
    }

    public override void enter()
    {
        m_stateController.changeState(StateDefinition.MAIN);
    }
}
