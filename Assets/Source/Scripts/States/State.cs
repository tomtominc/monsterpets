﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State
{
    public State(ApplicationController p_controller)
    {
        m_controller = p_controller;
        m_stateController = m_controller.getController<StateController>();
        m_viewController = m_controller.getController<ViewController>();
        m_dataController = m_controller.getController<DataController>();
        m_playerModel = m_dataController.playerModel;
    }

    public virtual void enter()
    {

    }

    public virtual void update()
    {

    }

    public virtual void exit()
    {

    }

    protected ApplicationController m_controller;
    protected StateController m_stateController;
    protected ViewController m_viewController;
    protected DataController m_dataController;
    protected PlayerModel m_playerModel;
}
