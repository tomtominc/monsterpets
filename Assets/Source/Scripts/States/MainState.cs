﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainState : State
{
    public MainState(ApplicationController p_controller)
        : base(p_controller)
    {
    }

    public override void enter()
    {
        m_viewController.open(ViewDefinition.NAVIGATION_BAR);

        if (!tutorialFinished)
        {
            tutorialFinished = true;
            m_stateController.changeState(StateDefinition.TUTORIAL_ONBOARDING);
        }
    }

    private bool tutorialFinished;
}
