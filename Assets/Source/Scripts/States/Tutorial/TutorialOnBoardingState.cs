﻿using UnityEngine;
using UnityEngine.UI;

public class TutorialOnBoardingState : TutorialState
{
    public TutorialOnBoardingState(ApplicationController p_controller)
        : base(p_controller)
    {

    }

    public override void enter()
    {
        m_navigationBar = m_viewController.open(ViewDefinition.NAVIGATION_BAR) as NavigationBar;
        m_navigationBar.enableScrolling(false);

        MaterialUI.TabPage mainMenuPage = m_navigationBar.tabView.pages[2];
        MainMenu mainMenu = mainMenuPage.GetComponentInChildren<MainMenu>();
        m_tutorialTrainer = mainMenu.trainers[0];

        m_dialogueView = m_viewController.open(ViewDefinition.DIALOGUE) as DialogueView;
        m_dialogueView.setDialogue(m_tutorialController.getTutorial(0));
        m_dialogueView.frameStartAction += OnFrameStart;
        m_dialogueView.completeAction += OnCompleteTutorial;
        m_dialogueView.startDialogue();
    }

    public override void exit()
    {
        m_navigationBar.enableScrolling(true);

        GraphicRaycaster raycaster = m_tutorialTrainer.gameObject.GetComponent<GraphicRaycaster>();
        Object.Destroy(raycaster);
        Canvas canvas = m_tutorialTrainer.gameObject.GetComponent<Canvas>();
        Object.Destroy(canvas);

        m_dialogueView.frameStartAction -= OnFrameStart;
        m_dialogueView.completeAction -= OnCompleteTutorial;

        m_viewController.close(ViewDefinition.DIALOGUE);
    }

    public virtual void OnFrameStart(int p_frame)
    {
        m_currentTutorialFrame = p_frame;

        if (m_currentTutorialFrame == 3)
        {
            Canvas canvas = m_tutorialTrainer.gameObject.AddComponent<Canvas>();
            canvas.overrideSorting = true;
            canvas.sortingOrder = 25;
            m_tutorialTrainer.gameObject.AddComponent<GraphicRaycaster>();

            m_tutorialTrainer.hireButton.purchaseAction += OnHireTrainer;

            RectTransform pointerTarget = m_tutorialTrainer.hireButton.transform as RectTransform;
            Vector2 pointerOffset = new Vector2(0.5f, -0.5f);
            m_dialogueView.showPointer(pointerTarget, pointerOffset);
        }
        else if (m_currentTutorialFrame == 6)
        {
            m_tutorialTrainer.adventureButton.onClick.AddListener(OnSendTrainer);

            RectTransform pointerTarget = m_tutorialTrainer.adventureButton.transform as RectTransform;
            Vector2 pointerOffset = new Vector2(0f, 0f);
            m_dialogueView.showPointer(pointerTarget, pointerOffset);
        }
        else if (m_currentTutorialFrame == 13)
        {
            m_navigationBar.enableInput(true);
            m_tutorialTrainer.levelUpButton.purchaseAction += OnLevelUpTrainer;

            RectTransform pointerTarget = m_tutorialTrainer.levelUpButton.transform as RectTransform;
            Vector2 pointerOffset = new Vector2(0.5f, -0.5f);
            m_dialogueView.showPointer(pointerTarget, pointerOffset);
        }
    }

    public virtual void OnCompleteTutorial()
    {
        m_stateController.changeState(StateDefinition.MAIN);
    }

    private void OnHireTrainer()
    {
        m_tutorialTrainer.hireButton.purchaseAction -= OnHireTrainer;
        m_dialogueView.hidePointer();
        m_dialogueView.continueDialogue();
    }

    private void OnSendTrainer()
    {
        if (m_currentTutorialFrame == 10)
        {
            InventoryItem coins = m_playerModel.inventory.getItem(Currency.COIN);

            if (coins.amount >= 10)
            {
                m_navigationBar.enableInput(false);
                m_tutorialTrainer.adventureButton.onClick.RemoveListener(OnSendTrainer);
                m_dialogueView.hidePointer();
                m_dialogueView.continueDialogue();
            }
        }
        else
        {
            m_dialogueView.skipDialogueType();
        }
    }

    private void OnLevelUpTrainer()
    {
        m_tutorialTrainer.levelUpButton.purchaseAction -= OnLevelUpTrainer;
        m_dialogueView.hidePointer();
        m_dialogueView.continueDialogue();
    }

    protected int m_currentTutorialFrame;
    protected DialogueView m_dialogueView;
    protected NavigationBar m_navigationBar;
    protected TrainerView m_tutorialTrainer;
}
