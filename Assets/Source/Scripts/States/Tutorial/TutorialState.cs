﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialState : State
{
    public TutorialState(ApplicationController p_controller)
        : base(p_controller)
    {
        m_tutorialController = m_controller.getController<TutorialController>();
    }

    protected TutorialController m_tutorialController;
}