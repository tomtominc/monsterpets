﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class NumberUtility
{
    public const long THOUSANDS = 1000;
    public const long MILLIONS = 1000000;
    public const long BILLIONS = 1000000000;
    public const long TRILLIONS = 1000000000;
    public const long QUADRILLIONS = 1000000000000;
    public const long QUINTILLIONS = 1000000000000000;
    public const long SEXTILLIONS = 1000000000000000000;
    public const long MAX_VALUE = long.MaxValue;


    public static string format(long p_number)
    {
        string l_format = p_number.ToString() + "C";

        if (p_number >= SEXTILLIONS)
        {
            l_format = (p_number / SEXTILLIONS).ToString() + "Z";
        }
        else if (p_number >= QUINTILLIONS)
        {
            l_format = (p_number / QUINTILLIONS).ToString() + "E";
        }
        else if (p_number >= QUADRILLIONS)
        {
            l_format = (p_number / QUADRILLIONS).ToString() + "P";
        }
        else if (p_number >= TRILLIONS)
        {
            l_format = (p_number / TRILLIONS).ToString() + "T";
        }
        else if (p_number >= BILLIONS)
        {
            l_format = (p_number / BILLIONS).ToString() + "B";
        }
        else if (p_number >= MILLIONS)
        {
            l_format = (p_number / MILLIONS).ToString() + "M";
        }
        else if (p_number >= THOUSANDS)
        {
            l_format = (p_number / THOUSANDS).ToString() + "K";
        }

        return l_format;
    }

    public static string timeFormat(float p_seconds)
    {
        TimeSpan l_time = TimeSpan.FromSeconds(p_seconds);
        return string.Format("{0:D2}:{1:D2}:{2:D2}", l_time.Hours, l_time.Minutes, l_time.Seconds);
    }
}
