﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDataEditor;

public class Formula : MonoBehaviour
{
    public static long getLevelUpCost(TrainerModel p_trainer, GDETrainerData p_data)
    {
        long l_initialCost = long.Parse(p_data.cost);
        long l_cost = l_initialCost * (int)Mathf.Pow(p_data.coefficient, p_trainer.level + 1);
        return l_cost;
    }

    public static long getRewardValue(TrainerModel p_trainer, GDETrainerData p_data)
    {
        long reward = 0;

        if (!string.IsNullOrEmpty(p_data.reward))
        {
            reward = long.Parse(p_data.reward);
        }

        return p_trainer.level * reward;
    }

    public static long getCaptureCost(TrainerModel p_trainer, GDETrainerData p_data)
    {
        long l_initialCost = long.Parse(p_data.cost);
        long l_cost = (long)Mathf.Pow(l_initialCost, p_trainer.monsters.Count + 1);
        return l_cost;
    }
}
