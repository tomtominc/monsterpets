using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSelection : ISelectable
{
    public string id;
    public float weight;

    public float AdjustedSelectionWeight
    {
        get; set;
    }

    public ItemSelection(string p_id, float p_weight)
    {
        id = p_id;
        weight = p_weight;
    }

    public float GetSelectionWeight()
    {
        return weight;
    }
}
