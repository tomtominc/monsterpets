﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameDataEditor;

public class Entity : MonoBehaviour
{
    [SerializeField]
    protected Image m_icon;
    [SerializeField]
    protected Text m_nameLabel;

    public void initialize(ApplicationController p_controller, IGDEData p_data)
    {
        m_controller = p_controller;
        m_dataController = m_controller.getController<DataController>();
        m_data = p_data;

        updateView();
    }

    protected virtual void updateView()
    {
        
    }

    protected virtual void localize()
    {
        
    }

    protected ApplicationController m_controller;
    protected DataController m_dataController;
    protected PlayerModel m_playerModel;
    protected IGDEData m_data;
}
