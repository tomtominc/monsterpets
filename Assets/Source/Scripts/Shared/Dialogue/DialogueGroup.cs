﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class DialogueGroup : ScriptableObject
{
    public List<Dialogue> dialogues;
}
