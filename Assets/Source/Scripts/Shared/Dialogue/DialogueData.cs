﻿
using UnityEngine;

[System.Serializable]
public class DialogueData
{
    public int theme;
    public string name;
    public string portrait;
    public ContinueType continueType;
    public string audio;
    [TextArea]
    public string text;
}
