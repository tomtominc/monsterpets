using System;
using UnityEngine;
using UnityEngine.UI;

public class DialogueContainer : MonoBehaviour
{
    [SerializeField]
    protected RectTransform m_contentHolder;
    [SerializeField]
    protected RectTransform m_content;
    [SerializeField]
    protected Text m_name;
    [SerializeField]
    protected Text m_dialogue;
    [SerializeField]
    protected float m_animateCharDelay;
    [SerializeField]
    protected AudioClip m_characterTypedSound;

    public virtual bool animating
    {
        get { return m_animating; }
    }

    public virtual bool showingDialogue
    {
        get { return m_showingDialogue; }
    }

    public virtual void initialize(DialogueView p_system)
    {
        m_system = p_system;
        m_typer = m_dialogue.GetComponent<TextTyper>();
        m_rect = m_contentHolder;
    }

    public virtual void animateDialogue(DialogueData p_data)
    {

    }

    public virtual void setDialogue(DialogueData p_data)
    {

    }

    public virtual void animateDialogueText(string p_text)
    {
        m_showingDialogue = true;
        m_animating = true;
        m_typer.TypeText(p_text);
    }

    public virtual void cancelAnimateDialogueText()
    {
        m_animating = false;
        m_showingDialogue = false;
        m_typer.Skip();
    }

    public virtual void OnCompleteTypeText()
    {
        m_animating = false;
        m_showingDialogue = false;
    }

    public virtual void OnTypedCharacter(string p_character)
    {
        if (m_characterTypedSound)
        {
            AudioSource.PlayClipAtPoint(m_characterTypedSound, new Vector3(0, 0, -10f));
        }
    }

    public virtual void animateOut(Action p_complete = null)
    {

    }

    protected DialogueView m_system;
    protected RectTransform m_rect;
    protected TextTyper m_typer;
    protected bool m_animating;
    protected bool m_showingDialogue;
    protected string m_currentDialogue;

}
