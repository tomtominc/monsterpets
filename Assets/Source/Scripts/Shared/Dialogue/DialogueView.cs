﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class DialogueView : View
{
    [SerializeField]
    private List<DialogueContainer> m_containers;
    [SerializeField]
    private RectTransform m_pointer;
    [SerializeField]
    private CanvasGroup m_canvasGroup;

    public event Action<int> frameStartAction = delegate { };
    public event Action completeAction = delegate { };

    public override void open()
    {
        m_pointer.gameObject.SetActive(false);

        for (int i = 0; i < m_containers.Count; i++)
        {
            m_containers[i].initialize(this);
        }
    }
    public void setDialogue(Dialogue p_dialogue)
    {
        m_dialogue = p_dialogue;
        m_queuedDialogue = new Queue<DialogueData>(m_dialogue.data);
    }

    public void startDialogue()
    {
        StartCoroutine(iterateDialogue());
    }

    public void continueDialogue()
    {
        m_continue = true;
    }

    public void skipDialogueType()
    {
        if (m_skipDialogueType)
        {
            m_continue = true;
        }

        m_skipDialogueType = true;
    }

    private IEnumerator iterateDialogue()
    {
        while (m_queuedDialogue.Count > 0)
        {
            DialogueData dialogueData = m_queuedDialogue.Dequeue();

            m_currentContainer = m_containers[dialogueData.theme];

            setContainer(dialogueData);

            m_continue = dialogueData.continueType == ContinueType.MOUSE_DOWN;
            frameStartAction(m_currentFrame);

            if (m_continue)
            {
                bool _continue = false;

                while (!_continue)
                {
                    bool isInputRecieved = Input.GetMouseButtonUp(0);

                    _continue = isInputRecieved;// && !m_currentContainer.animating;

                    if (_continue && m_currentContainer.showingDialogue)
                    {
                        m_currentContainer.cancelAnimateDialogueText();
                        _continue = false;
                    }

                    yield return null;
                }
            }
            else
            {
                while (!m_continue)
                {
                    if (m_skipDialogueType)
                    {
                        m_currentContainer.cancelAnimateDialogueText();
                    }

                    yield return null;
                }

                // if you're typing the dialogue lets complete it still.
                if (m_currentContainer.showingDialogue)
                {
                    m_currentContainer.cancelAnimateDialogueText();
                }

                // keep this while loop in case it takes a little bit before 
                // the complete action gets fired.
                while (m_currentContainer.showingDialogue)
                {
                    yield return null;
                }

                m_skipDialogueType = false;
            }

            yield return new WaitForEndOfFrame();

            m_currentFrame++;
        }

        onDialogueEnd();
    }

    public void showPointer(RectTransform p_target, Vector3 p_offset)
    {
        m_pointer.gameObject.SetActive(true);
        m_pointer.position = p_target.position + p_offset;
        m_pointer.DOSizeDelta(m_pointer.sizeDelta * 1.2f, 0.25f)
                 .SetLoops(-1, LoopType.Yoyo);
    }

    public void hidePointer()
    {
        m_pointer.gameObject.SetActive(false);
    }

    private void setContainer(DialogueData p_data)
    {
        if (m_lastDialogue != null && m_lastDialogue.name == p_data.name)
        {
            m_currentContainer.setDialogue(p_data);
        }
        else
        {
            m_currentContainer.animateDialogue(p_data);
        }

        m_lastDialogue = p_data;
    }

    private void onDialogueEnd()
    {
        for (int i = 0; i < m_containers.Count; i++)
        {
            m_containers[i].animateOut();
        }

        completeAction();
    }

    public override void close()
    {
        m_canvasGroup.DOFade(0f, 1f).OnComplete(base.close);
    }

    private int m_currentFrame;
    private bool m_continue;
    private bool m_skipDialogueType;
    private DialogueContainer m_currentContainer;
    private Dialogue m_dialogue;
    private DialogueData m_lastDialogue;
    private Queue<DialogueData> m_queuedDialogue;
    private int m_currentDialogueDataIndex;
}
