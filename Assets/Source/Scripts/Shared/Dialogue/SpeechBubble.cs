using System;
using DG.Tweening;
using UnityEngine;

public class SpeechBubble : DialogueContainer
{
    [SerializeField]
    private Animator m_picture;

    public override void initialize(DialogueView p_system)
    {
        base.initialize(p_system);
        m_content.anchoredPosition = getOutAnchorPos();
    }

    public override void animateDialogue(DialogueData p_data)
    {
        m_animating = true;

        if (m_content.anchoredPosition.y < 0)
        {
            setDialogue(p_data);
            m_content.DOAnchorPos(Vector2.zero, 1f)
                     .SetEase(Ease.OutBack)
                     .OnComplete(() => m_animating = false);
        }
        else
        {
            m_content.DOAnchorPos(getOutAnchorPos(), 0.5f)
                     .OnComplete
                     (() =>
            {
                setDialogue(p_data);
                m_content.DOAnchorPos(Vector2.zero, 0.5f)
                         .OnComplete(() => m_animating = false);
            });
        }
    }

    public override void setDialogue(DialogueData p_data)
    {
        if (m_content.anchoredPosition.y < 0)
        {
            m_content.DOAnchorPos(Vector2.zero, 0.5f)
                     .OnComplete(() => m_animating = false);
        }
        if (containsClip(p_data.portrait))
        {
            m_picture.Play(p_data.portrait);
        }
        else if (m_picture != null)
        {
            m_picture.Play("Unknown");
        }

        if (m_name != null)
        {
            m_name.text = p_data.name;
        }

        animateDialogueText(p_data.text);
    }

    public override void animateOut(Action p_complete = null)
    {
        m_animating = true;
        m_content.DOAnchorPos(getOutAnchorPos(), 0.5f)
                 .OnComplete(() =>
        {
            m_animating = false;

            if (p_complete != null)
                p_complete();
        });
    }

    protected virtual Vector2 getOutAnchorPos()
    {
        return new Vector2(0f, -m_rect.sizeDelta.y * 1.2f);
    }

    protected virtual bool containsClip(string p_clipName)
    {
        if (m_picture == null)
            return false;

        bool p_contains = false;

        for (int i = 0; i < m_picture.runtimeAnimatorController.animationClips.Length; i++)
        {
            if (m_picture.runtimeAnimatorController.animationClips[i].name.Equals(p_clipName))
            {
                p_contains = true;
                break;
            }
        }

        return p_contains;
    }
}
