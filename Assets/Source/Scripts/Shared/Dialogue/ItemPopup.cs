using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ItemPopup : DialogueContainer
{
    [SerializeField]
    private Animator m_picture;

    [SerializeField]
    protected CanvasGroup m_contentCanvas;

    public override void initialize(DialogueView p_system)
    {
        base.initialize(p_system);

        m_startScale = new Vector3(0.8f, 0.8f, 1f);
        m_contentCanvas.alpha = 0;
        m_content.localScale = m_startScale;
    }

    public override void animateDialogue(DialogueData p_data)
    {
        m_animating = true;

        if (System.Math.Abs(m_contentCanvas.alpha) < float.Epsilon)
        {
            setDialogue(p_data);

            m_contentCanvas.DOFade(1f, 0.5f);
            m_content.DOScale(Vector3.one, 0.5f).OnComplete(() => m_animating = false);
        }
        else
        {
            m_contentCanvas.DOFade(0f, 0.5f);
            m_content.DOScale(m_startScale, 0.5f)
                     .OnComplete(() =>
            {
                setDialogue(p_data);

                m_contentCanvas.DOFade(1f, 0.5f);
                m_content.DOScale(Vector3.one, 0.5f).OnComplete(() => m_animating = false);
            });
        }
    }

    public override void setDialogue(DialogueData p_data)
    {
        if (containsClip(p_data.portrait))
        {
            m_picture.Play(p_data.portrait);
        }
        else if (m_picture != null)
        {
            m_picture.Play("Unknown");
        }

        m_dialogue.text = p_data.text;
    }

    protected virtual bool containsClip(string p_clipName)
    {
        if (m_picture == null)
            return false;

        bool p_contains = false;

        for (int i = 0; i < m_picture.runtimeAnimatorController.animationClips.Length; i++)
        {
            if (m_picture.runtimeAnimatorController.animationClips[i].name.Equals(p_clipName))
            {
                p_contains = true;
                break;
            }
        }

        return p_contains;
    }

    protected Vector3 m_startScale;

}
