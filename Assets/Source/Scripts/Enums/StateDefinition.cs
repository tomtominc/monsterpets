﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateDefinition
{
    LOADING,
    MAIN,
    CAPSULE_MACHINE,
    DEX,
    INVENTORY,
    MORPH,
    LOOT,
    RETURN,
    SHOP,
    TUTORIAL_ONBOARDING
}
