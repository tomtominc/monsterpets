﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ViewDefinition
{
    LOADING,
    MAIN,
    CAPSULE_MACHINE,
    DEX,
    INVENTORY,
    MONSTER_INFO,
    MONSTER_LIST,
    MUNCH_LIST,
    MUNCH_INFO,
    POPUP,
    MORPH,
    LOOT,
    RETURN,
    SHOP,
    MAIN_HUD,
    NAVIGATION_BAR,
    DIALOGUE
}
