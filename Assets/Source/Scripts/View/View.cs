﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class View : MonoBehaviour
{
    public virtual void initialize(ApplicationController p_controller)
    {
        m_controller = p_controller;
        m_viewController = m_controller.getController<ViewController>();
        m_dataController = m_controller.getController<DataController>();
        m_timeController = m_controller.getController<TimeController>();
        m_playerModel = m_dataController.playerModel;
        m_rect = GetComponent<RectTransform>();

        addListeners();
    }

    public virtual void open()
    {

    }

    public virtual void close()
    {
        removeListeners();
        DestroyObject(gameObject);
    }

    public virtual void addListeners()
    {

    }

    public virtual void removeListeners()
    {

    }

    protected ApplicationController m_controller;
    protected ViewController m_viewController;
    protected DataController m_dataController;
    protected TimeController m_timeController;
    protected PlayerModel m_playerModel;
    protected RectTransform m_rect;
}
