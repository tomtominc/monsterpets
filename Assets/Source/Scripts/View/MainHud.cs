﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainHud : View
{
    [SerializeField]
    private RectTransform m_coinCurrencyRect;
    [SerializeField]
    private Text m_coinCurrencyLabel;

    public void setCoins(long m_value)
    {
        string l_value = NumberUtility.format(m_value);
        m_coinCurrencyLabel.text = l_value;
    }
}
