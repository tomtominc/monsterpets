﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MaterialUI;

public class NavigationBar : View
{
    [SerializeField]
    private RectTransform m_navigationContent;
    [SerializeField]
    private ToggleGroup m_navigationToggleGroup;
    [SerializeField]
    private ScrollRect m_navigationScrollRect;
    [SerializeField]
    private TabView m_tabView;
    [SerializeField]
    private CanvasGroup m_canvasGroup;

    public TabView tabView
    {
        get { return m_tabView; }
    }

    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);

        configureNavigations();
    }

    private void Update()
    {
        if (m_tabView.currentPage != m_currentPage)
        {
            if (m_navButtons == null)
            {
                m_navButtons = new NavigationButton[m_tabView.tabs.Length];

                for (int i = 0; i < m_tabView.tabs.Length; i++)
                {
                    NavigationButton button = m_tabView.tabs[i].GetComponentInChildren<NavigationButton>();
                    button.initialize(m_controller);
                    m_navButtons[i] = button;
                }
            }

            NavigationButton disabledButton = getNavButton(m_currentPage);
            disabledButton.updateView(false);
            m_currentPage = m_tabView.currentPage;
            NavigationButton enableButton = getNavButton(m_currentPage);
            enableButton.updateView(true);
        }
    }

    private NavigationButton getNavButton(int p_index)
    {
        return m_navButtons[p_index];
    }

    public void configureNavigations()
    {
        m_navViews = new NavigationView[m_tabView.pages.Length];

        for (int i = 0; i < m_tabView.pages.Length; i++)
        {
            NavigationView view = m_tabView.pages[i].GetComponentInChildren<NavigationView>();
            view.initialize(m_controller);
            view.open();

            m_navViews[i] = view;
        }

        m_mainMenu = m_tabView.pages[2].GetComponentInChildren<MainMenu>();
    }

    public void enableInput(bool p_enabled)
    {
        m_canvasGroup.interactable = p_enabled;
    }

    public void enableScrolling(bool p_enabled)
    {
        m_tabView.pagesScrollRect.enabled = p_enabled;
        m_mainMenu.trainerScrollView.enabled = p_enabled;
    }

    protected MainMenu m_mainMenu;
    protected NavigationView[] m_navViews;
    protected NavigationButton[] m_navButtons;
    protected int m_currentPage;
    protected int m_lastPage;

}
