﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDataEditor;
using UnityEngine.UI;
using DG.Tweening;

public class MainMenu : NavigationView
{
    [SerializeField]
    private RectTransform m_trainerContent;
    [SerializeField]
    private RectTransform m_trainerViewPrefab;
    [SerializeField]
    private MonsterButtonView m_monsterButtonView;
    [SerializeField]
    private ScrollRect m_trainerScrollView;

    public List<TrainerView> trainers
    {
        get { return m_trainerViews; }
    }

    public ScrollRect trainerScrollView
    {
        get { return m_trainerScrollView; }
    }

    public override void initialize(ApplicationController p_controller)
    {
        base.initialize(p_controller);

        m_monsterButtonView.initialize(m_controller);

        m_playerModel.inventory.modifyItem(Currency.COIN, 10);
        InventoryItem l_coins = m_playerModel.inventory.getItem(Currency.COIN);
        m_hud = m_viewController.open(ViewDefinition.MAIN_HUD) as MainHud;
        m_hud.setCoins(l_coins.amount);

        m_timeController.timerFinished += onTimerFinished;

        configureTrainerViews();
    }

    public void configureTrainerViews()
    {
        m_trainerViews = new List<TrainerView>();
        Vector2 l_size = Vector2.zero;

        for (int i = 0; i < m_playerModel.trainers.Count; i++)
        {
            TrainerModel l_trainer = m_playerModel.trainers[i];
            GDETrainerData l_data = new GDETrainerData(l_trainer.name);
            RectTransform l_trainerViewRect = Instantiate(m_trainerViewPrefab);
            l_trainerViewRect.SetParent(m_trainerContent, false);
            TrainerView l_trainerView = l_trainerViewRect.GetComponent<TrainerView>();
            l_trainerView.Initialize(m_controller, l_trainer, l_data);
            l_trainerView.adventureAction += onAdventureAction;
            l_trainerView.purchaseTrainer += onPurchaseAction;
            l_trainerView.insufficentAction += onInsufficientAction;
            l_trainerView.monsterShow += onMonsterShow;
            l_trainerView.purchaseCaptureMonster += onCapturePurchased;

            m_trainerViews.Add(l_trainerView);
        }

        resizeTrainerContent(false);
    }

    private void reloadPurchaseButtons()
    {
        for (int i = 0; i < m_trainerViews.Count; i++)
        {
            m_trainerViews[i].configurePurchaseButtons();
        }
    }
    private void resizeTrainerContent(bool p_animate)
    {
        float l_height = 0;
        VerticalLayoutGroup l_layout = m_trainerContent.GetComponent<VerticalLayoutGroup>();

        for (int i = 0; i < m_trainerContent.childCount; i++)
        {
            RectTransform l_trainerRect = m_trainerContent.GetChild(i) as RectTransform;
            LayoutElement l_layoutElement = l_trainerRect.GetComponent<LayoutElement>();
            l_height += l_layout.spacing + /*TrainerView.TRAINER_UPPER_CONTENT_HEIGHT +*/ l_layoutElement.preferredHeight;
        }

        m_trainerContent.sizeDelta = new Vector2(m_trainerContent.sizeDelta.x, l_height);
    }


    private void onAdventureAction(TrainerView l_trainerView)
    {
        AdventureTimer l_timer = m_timeController.getTimer(l_trainerView.trainer, l_trainerView.data);

        Debug.LogFormat("Timer {0}", l_timer);
        l_timer.active = true;
    }

    private void onPurchaseAction(TrainerView p_trainerView)
    {
        TrainerModel l_trainer = p_trainerView.trainer;

        l_trainer.level++;

        if (!l_trainer.hired)
        {
            l_trainer.hired = true;
            GDEMonsterData l_data = m_dataController.getRandomMonster();
            l_trainer.monsters.Add(l_data.id);
        }

        InventoryItem l_coins = m_playerModel.inventory.getItem(Currency.COIN);
        m_hud.setCoins(l_coins.amount);

        p_trainerView.reload();
        reloadPurchaseButtons();
        resizeTrainerContent(true);
    }

    private void onInsufficientAction()
    {

    }

    private void onCapturePurchased(TrainerView p_trainerView)
    {
        TrainerModel l_trainer = p_trainerView.trainer;

        GDEMonsterData l_data = m_dataController.getRandomMonster();
        l_trainer.monsters.Add(l_data.id);

        InventoryItem l_coins = m_playerModel.inventory.getItem(Currency.COIN);
        m_hud.setCoins(l_coins.amount);

        p_trainerView.reload();

        reloadPurchaseButtons();
    }

    private void onTimerFinished(AdventureTimer p_timer)
    {
        Inventory l_inventory = m_playerModel.inventory;
        l_inventory.modifyItem(Currency.COIN, p_timer.reward);
        InventoryItem l_coins = m_playerModel.inventory.getItem(Currency.COIN);
        m_hud.setCoins(l_coins.amount);

        reloadPurchaseButtons();
    }

    private void onMonsterShow(TrainerView p_view, TrainerPetView p_monsterView)
    {
        m_trainerScrollView.enabled = false;
        Tweener l_snapTween = snapTo((RectTransform)p_view.transform);

        l_snapTween.OnComplete(() =>
        {
            m_monsterButtonView.closeAction += onCloseMonsterButtonView;
            m_monsterButtonView.gameObject.SetActive(true);
            m_monsterButtonView.open(p_monsterView);
        });
    }

    private void onCloseMonsterButtonView()
    {
        m_monsterButtonView.closeAction -= onCloseMonsterButtonView;
        m_trainerScrollView.enabled = true;
        m_monsterButtonView.gameObject.SetActive(false);
    }

    public Tweener snapTo(RectTransform target)
    {
        Canvas.ForceUpdateCanvases();

        Vector2 l_contentPoint = (Vector2)m_trainerScrollView.transform.InverseTransformPoint(m_trainerContent.position);
        Vector2 l_itemPoint = (Vector2)m_trainerScrollView.transform.InverseTransformPoint(target.position);
        Vector2 l_targetPoint = l_contentPoint - l_itemPoint;

        return m_trainerContent.DOAnchorPos(l_targetPoint, 2000f).SetSpeedBased(true);
    }

    private List<TrainerView> m_trainerViews;
    private MainHud m_hud;
}
