﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NavigationView : View
{
    [SerializeField]
    protected Toggle m_toggle;

    public virtual void configureNavigation(NavigationBar p_navigationBar,
                                            NavigationButton p_navigationButton,
                                            RectTransform p_navigationContent,
                                            ToggleGroup p_group)
    {
        m_navigationBar = p_navigationBar;
        m_navigationButton = p_navigationButton;
        m_navigationContent = p_navigationContent;
        m_toggle.group = p_group;

        m_rect.SetParent(m_navigationContent, false);

        m_toggle.onValueChanged.AddListener(onToggle);
    }

    public virtual void forceOn()
    {
        m_toggle.isOn = true;
    }

    public virtual void onToggle(bool p_isOn)
    {
        m_navigationButton.updateView(p_isOn);
    }

    protected NavigationBar m_navigationBar;
    protected NavigationButton m_navigationButton;
    protected RectTransform m_navigationContent;
    protected bool m_updateView;
}
